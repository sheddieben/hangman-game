﻿namespace SheddyBenHangman.Core.Enums
{
    //Enum to determine whether the player has losed or won the game, or continue playing
    public enum GameStatus
    {
        Lose,
        Win,
        Continue
    }
}
