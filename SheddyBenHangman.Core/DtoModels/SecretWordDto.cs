﻿using System;
using System.Collections.Generic;
using SheddyBenHangman.Core.Model;

namespace SheddyBenHangman.Core.DtoModels
{
    //This class holds all the secret words to be guessed by the players in the Hangman Game
    public class SecretWordDto : List<SecretWord>
    {
        public SecretWordDto()
        {
            Add(new SecretWord() { Details = "HONEY" });
            Add(new SecretWord() { Details = "HOSTEL" });
            Add(new SecretWord() { Details = "HOUSE" });
            Add(new SecretWord() { Details = "INSPECTOR" });
            Add(new SecretWord() { Details = "OLIVER" });
            Add(new SecretWord() { Details = "PERIMETER" });
            Add(new SecretWord() { Details = "INVESTIGATION" });
            Add(new SecretWord() { Details = "BEAUTIFUL" });
            Add(new SecretWord() { Details = "SYSTEMS" });
            Add(new SecretWord() { Details = "HOLIDAY" });
            Add(new SecretWord() { Details = "COLLECTION" });
            Add(new SecretWord() { Details = "DRESS" });
            Add(new SecretWord() { Details = "SUIT" });
            Add(new SecretWord() { Details = "CHRIS" });
            Add(new SecretWord() { Details = "TROUSER" });
            Add(new SecretWord() { Details = "HILLS" });
            Add(new SecretWord() { Details = "MOUNTAIN" });
            Add(new SecretWord() { Details = "FOUNDATION" });
            Add(new SecretWord() { Details = "RESPECT" });
            Add(new SecretWord() { Details = "MICROSOFT" });
            Add(new SecretWord() { Details = "BARRACK" });
            Add(new SecretWord() { Details = "MOVIE" });
            Add(new SecretWord() { Details = "CHRIS" });
            Add(new SecretWord() { Details = "FORENSICS" });
            Add(new SecretWord() { Details = "AUDIT" });
            Add(new SecretWord() { Details = "MAGAZINE" });
            Add(new SecretWord() { Details = "BRITAIN" });
            Add(new SecretWord() { Details = "LONDON" });
            Add(new SecretWord() { Details = "DAVE" });
            Add(new SecretWord() { Details = "OXFORD" });
        }

        //select random word from the list above
        public SecretWord SecretWord
        {
            get
            {
                var randomLetter = new Random();
                var index = (int)(randomLetter.NextDouble() * Count);
                var word = this[index];
                word.Details = word.Details.ToUpper();
                return word;
            }
        }
    }

   
}
