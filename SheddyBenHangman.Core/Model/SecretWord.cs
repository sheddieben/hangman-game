﻿namespace SheddyBenHangman.Core.Model
{
    public class SecretWord
    {
        public SecretWord()
        {
            
        }
        public SecretWord(string details)
        {
            Details = details;
        }
        public string Details;
        public int SecretWordLength => Details.Length;
    }
}
