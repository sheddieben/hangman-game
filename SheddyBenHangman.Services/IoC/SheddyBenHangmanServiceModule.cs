﻿using Ninject.Modules;
using SheddybenHangman.ServicesContracts.IServices;

namespace SheddyBenHangman.Services.IoC
{
    public class SheddyBenHangmanServiceModule : NinjectModule
    {
        public override void Load()
        {
            //Services
            Bind<IPlayGame>().To<IPlayGame>();
        }
    }
}
