﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using SheddybenHangman.ServicesContracts.IServices;
using SheddyBenHangman.Core.Enums;
using SheddyBenHangman.Core.Model;

namespace SheddyBenHangman.Services.Services
{
    public class PlayGame : IPlayGame
    {
        public SecretWord SecretWord { get; set; }
        private List<string> _userGuessedAndFound;
        private readonly List<string> _userGuessedLetters;
        private List<string> _userMissedLetters;

        public PlayGame()
        {
            _userGuessedAndFound = new List<string>();
            _userGuessedLetters = new List<string>();
            _userMissedLetters = new List<string>();
        }

        public void DisplayGameInstructions()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("================================================================================    ");
            Console.WriteLine("                       Welcome to  the Hangman Game                                 ");
            Console.WriteLine("================================================================================    ");
            Console.WriteLine("  Game Instructions:                                                                ");
            Console.WriteLine("        1. A secret word has been chosen.                                           ");
            Console.WriteLine("        2. A blank line representing each letter has been drawn below.              ");
            Console.WriteLine("        3. You're supposed to guess letters to fill in the blanks.                  ");
            Console.WriteLine("        4. Correct letter will be filled in the blanks.                             ");
            Console.WriteLine("        5. Every incorrect guess leads to a part of the Hangman been drawn.         ");
            Console.WriteLine("        6. You win when the letters guessed match the secret word.                  ");
            Console.WriteLine("        7. You have only seven chances to play the game.                            ");
        }
        public void Play()
        {
            //compare word guessed by user against the word picked randomly
            _userGuessedAndFound = new List<string>();
            for (var i = 0; i < SecretWord.SecretWordLength; i++)
            {
                _userGuessedAndFound.Add(" _ ");
            }

            for (var i = 0; i < SecretWord.SecretWordLength; i++)
            {
                var letter = SecretWord.Details.Substring(i, 1);
                if(_userGuessedLetters.Count <= 0) continue;
                foreach (var userGuessedLetter in _userGuessedLetters)
                {
                    if (!letter.Equals(userGuessedLetter.Trim().ToUpper())) continue;
                    _userGuessedAndFound.RemoveAt(i);
                    _userGuessedAndFound.Insert(i, " " + letter + " ");
                }
            }
            DrawHangMan();
            Console.WriteLine(BuildString(_userGuessedAndFound, false));
            Console.WriteLine();
        }

        public bool AddGuessedLetters(char letter)
        {
            if (char.IsDigit(letter)) 
            {
                Console.WriteLine();
                Console.WriteLine("'" + letter.ToString().ToUpper() + "' is not a valid letter");
                return false;
            }
            if (!_userGuessedLetters.Contains(letter.ToString().ToUpper())) 
            {
                _userGuessedLetters.Add(letter.ToString().ToUpper());
                Console.WriteLine();
                Console.WriteLine("Letters guessed by you : " + BuildString(_userGuessedLetters, true));
                return true;
            }
            Console.WriteLine();
            Console.WriteLine("Opps!.., you already guessed the letter '" + letter.ToString().ToUpper() + "'");
            return false;
        }

        public bool IsLetterOnly(char letter)
        {
            if (char.IsDigit(letter))
            {
                Console.WriteLine();
                Console.WriteLine("'" + letter.ToString().ToUpper() + "' is not a valid letter");
            }
            return false;
        }

        public bool CheckTheGuessedLetter(string letter)
        {
            for (var i = 0; i < SecretWord.SecretWordLength; i++)
            {
                var letterSplitter = SecretWord.Details.Substring(i, 1).ToUpper();
                if (letterSplitter.Equals(letter.Trim().ToUpper()))
                {
                    return true;
                }
            }
            return false;
        }

        public string BuildString(IEnumerable<string> inPutString, bool space)
        {
            var outPut = new StringBuilder();
            outPut = inPutString.Aggregate(outPut, (current, item) => space ? current.Append(item.ToString().ToUpper() + " ") : current.Append(item.ToString().ToUpper()));
            return outPut.ToString();
        }
        

        public void DrawHangMan()
        {

            _userMissedLetters = new List<string>();
            foreach (string item in _userGuessedLetters.Where(item => !CheckTheGuessedLetter(item)))
            {
                _userMissedLetters.Add(item);
            }

            Console.WriteLine();
            Console.ForegroundColor = ConsoleColor.Cyan;
            if (_userMissedLetters.Count == 1)
            {
                Console.WriteLine("    _______");
                Console.WriteLine("   |  _____|");
                Console.WriteLine("   |*|     |");
                Console.WriteLine("   |*|     O");
                Console.WriteLine("   |*|");
                Console.WriteLine("   |*|");
                Console.WriteLine("   |*|");
                Console.WriteLine("   |*|");
                Console.WriteLine("___|*|___");
                Console.WriteLine("_________");
                Console.WriteLine("You have 6 more chances to win.");
            }
            else if (_userMissedLetters.Count == 2)
            {
                Console.WriteLine("    _______");
                Console.WriteLine("   |* _____|");
                Console.WriteLine("   |*|     |");
                Console.WriteLine("   |*|     O");
                Console.WriteLine("   |*|     |");
                Console.WriteLine("   |*|");
                Console.WriteLine("   |*|");
                Console.WriteLine("   |*|");
                Console.WriteLine("___|*|___");
                Console.WriteLine("_________");
                Console.WriteLine("You have 5 more chances to win.");
            }
            else if (_userMissedLetters.Count == 3)
            {
                Console.WriteLine("    _______");
                Console.WriteLine("   |* _____|");
                Console.WriteLine("   |*|     |");
                Console.WriteLine("   |*|     O");
                Console.WriteLine("   |*|    \\|");
                Console.WriteLine("   |*|");
                Console.WriteLine("   |*|");
                Console.WriteLine("   |*|");
                Console.WriteLine("___|*|___");
                Console.WriteLine("_________");
                Console.WriteLine("You have 4 more chances to win.");
            }
            else if (_userMissedLetters.Count == 4)
            {
                Console.WriteLine("    _______");
                Console.WriteLine("   |* _____|");
                Console.WriteLine("   |*|     |");
                Console.WriteLine("   |*|     O");
                Console.WriteLine("   |*|    \\|/");
                Console.WriteLine("   |*|");
                Console.WriteLine("   |*|");
                Console.WriteLine("   |*|");
                Console.WriteLine("___|*|___");
                Console.WriteLine("_________");
                Console.WriteLine("You have 3 more chances to win.");

            }
            else if (_userMissedLetters.Count == 5)
            {
                Console.WriteLine("    _______");
                Console.WriteLine("   |* _____|");
                Console.WriteLine("   |*|     |");
                Console.WriteLine("   |*|     O");
                Console.WriteLine("   |*|    \\|/");
                Console.WriteLine("   |*|     |");
                Console.WriteLine("   |*|");
                Console.WriteLine("   |*|");
                Console.WriteLine("___|*|___");
                Console.WriteLine("_________");
                Console.WriteLine("You have 2 more chances to win.");
            }
            else if (_userMissedLetters.Count == 6)
            {
                Console.WriteLine("    _______");
                Console.WriteLine("   |* _____|");
                Console.WriteLine("   |*|     |");
                Console.WriteLine("   |*|     O");
                Console.WriteLine("   |*|    \\|/");
                Console.WriteLine("   |*|     |");
                Console.WriteLine("   |*|    /");
                Console.WriteLine("   |*|");
                Console.WriteLine("___|*|___");
                Console.WriteLine("_________");
                Console.WriteLine("You have 1 more chances to win.");
            }
            else if (_userMissedLetters.Count == 7)
            {
                Console.WriteLine("    ________");
                Console.WriteLine("   |* ______|");
                Console.WriteLine("   |*|      |");
                Console.WriteLine("   |*|      O");
                Console.WriteLine("   |*|     \\|/");
                Console.WriteLine("   |*|      |");
                Console.WriteLine("   |*|     / \\");
                Console.WriteLine("   |*|");
                Console.WriteLine("___|*|___");
                Console.WriteLine("_________");
            }
            else
                Console.WriteLine();
            Console.WriteLine();
        }

        public GameStatus Result()
        {
            if (_userMissedLetters.Count == 7)
            {
                return GameStatus.Lose;
            }
            return SecretWord.Details.ToUpper().Equals(BuildString(_userGuessedAndFound, false).Replace(" ", "")) ? GameStatus.Win : GameStatus.Continue;
        }


        public void AnimateText(string word, int delay)
        {
            for (var i = 0; i < 5; i++)
            {
                DisplayAnimatedText(word, delay, true);
                DisplayAnimatedText(word, 450, false);
            }
        }

        public void DisplayAnimatedText(string word, int delay, bool visible)
        {
            if (visible) Console.Write(word);
            else
                for (var i = 0; i < word.Length; i++)
                    Console.Write(" ");
            Console.CursorLeft -= word.Length;
            Thread.Sleep(delay);
        }
    }
}
