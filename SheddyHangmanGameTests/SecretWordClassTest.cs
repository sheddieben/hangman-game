﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SheddyBenHangman.Core.DtoModels;
using SheddyBenHangman.Core.Model;

namespace SheddyHangmanGameTests
{
    [TestClass]
    public class SecretWordClassTest
    {
        [TestMethod]
        public void LoanWordsFormSecretWords()
        {
            var expected = new SecretWordDto
                           {
                               new SecretWord() {Details = "AUDIT"},
                               new SecretWord() { Details = "HOUSE" },
                               new SecretWord() {Details = "BARRACK"},
                               new SecretWord() {Details = "MAGAZINE"},
                               new SecretWord() {Details = "MICROSOFT"}
                           };
            var actualSecretWords = new SecretWordDto();
            Assert.AreEqual(expected.ToString(), actualSecretWords.ToString());
        }
    }
}
