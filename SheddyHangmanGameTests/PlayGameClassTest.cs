﻿using System;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SheddyBenHangman.Services.Services;

namespace SheddyHangmanGameTests
{
    [TestClass]
    public class PlayGameClassTest
    {
        [TestMethod]
        public void TestLettersOnly()
        {
            var playGame = new PlayGame();
            var letter = Convert.ToChar("9");
            var result = playGame.IsLetterOnly(letter);
            Assert.AreEqual(false, result);
        }

        [TestMethod]
        public void DisplayInstructionsMessageTest()
        {
            using (var writer = new StringWriter())
            {
                var playGame = new PlayGame();
                Console.SetOut(writer);
                playGame.DisplayGameInstructions();
                writer.Flush();
                var result = writer.GetStringBuilder().ToString();
                

                const string expectedOne = "================================================================================    \r\n" +
                                           "                       Welcome to  the Hangman Game                                 " +
                                           "\r\n================================================================================   " +
                                           " \r\n  Game Instructions:                                                              " +
                                           "  \r\n        1. A secret word has been chosen.                                           \r\n      " +
                                           "  2. A blank line representing each letter has been drawn below.              \r\n       " +
                                           " 3. You're supposed to guess letters to fill in the blanks.                  \r\n      " +
                                           "" +
                                           "  4. Correct letter will be filled in the blanks.                             \r\n      " +
                                           "  5. Every incorrect guess leads to a part of the Hangman been drawn.         \r\n       " +
                                           " 6. You win when the letters guessed match the secret word.                  \r\n       " +
                                           " 7. You have only seven chances to play the game.                            \r\n";
                Assert.AreEqual(expectedOne, result);
            }
        }
    }
}
