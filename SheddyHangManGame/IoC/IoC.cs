﻿using Ninject;
using Ninject.Modules;
using Ninject.Parameters;
using SheddyBenHangman.Services.IoC;

namespace SheddyHangManGame.IoC
{
    public class IoC
    {
        private static IKernel _kernel;
        static IKernel NinjectKernel => _kernel ?? (_kernel = new StandardKernel());

        public static void Init()
        {
            NinjectKernel.Load(new INinjectModule[] { new SheddyBenHangmanServiceModule(), });
        }

        public static T Resolve<T>() where T : class
        {
            if (_kernel == null) Init();

            return _kernel.Get<T>();
        }

        public static T Resolve<T>(ConstructorArgument constructorArgument) where T : class
        {
            if (_kernel == null) Init();

            return _kernel.Get<T>(constructorArgument);
        }
    }
}
