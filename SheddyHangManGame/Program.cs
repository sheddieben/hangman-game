﻿using System;
using SheddyBenHangman.Core.DtoModels;
using SheddyBenHangman.Core.Enums;
using SheddyBenHangman.Services.Services;

namespace SheddyHangManGame
{
    public class Program
    {
        public static void Main(string[] args)
        {
            
            InitializeIoC();
            var playgame = new PlayGame();
            playgame.DisplayGameInstructions();
            string answer;
            do
            {
                Console.WriteLine();
                answer = PlayGame();
            } while (answer.ToUpper().Equals("Y", StringComparison.InvariantCultureIgnoreCase));
        }
        private static string PlayGame()
        {
            var words = new SecretWordDto();
            var pickedWord = words.SecretWord;
            var playGame = new PlayGame
            {
                SecretWord = pickedWord
            };
            ConsoleKeyInfo answer;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Hint: The secret word has " + pickedWord.SecretWordLength + " letters");
            Console.WriteLine();
            for (var i = 0; i < pickedWord.SecretWordLength; i++)
            {
                Console.Write(" _ ");
            }
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine();
            while (playGame.Result() == GameStatus.Continue)
            {
                Console.Write("Select a letter to fill in the blanks.");
                var guessedLetter = Console.ReadKey();
                if (playGame.AddGuessedLetters(guessedLetter.KeyChar))
                    playGame.Play();
            }
            if (playGame.Result() == GameStatus.Lose)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("You losed the game!!!...");
                
                playGame.AnimateText("The secret word was '" + pickedWord.Details.ToUpper() + "'", 800);
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine("Do you want to play again ? Y/N");
                answer = Console.ReadKey();
                return answer.KeyChar.ToString();
            }
            Console.ForegroundColor = ConsoleColor.Magenta;
            playGame.AnimateText("Hey,....You won. Congratulations!!!", 800);
            Console.WriteLine("Would you like to play again ? Y/N");
            answer = Console.ReadKey();
            return answer.KeyChar.ToString();
        }

        private static void InitializeIoC()
        {
            IoC.IoC.Init();
        }
    }
}
