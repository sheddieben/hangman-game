Welcome to the Hangman Game
===========================================================
This is a console application that implements the hangman game.
To run this application click on the SheddyHangManGame.sln

Project Structure:
===========================================================
1. SheddyBenHangman.Core - A library that contains the enum and models classes.
2. SheddyBenHangman.Services - A library that contains the services.
3. SheddyBenHangman.ServiceContracts - A library that contains the interfaces for various services.
4. SheddyHangManGame - Console Application with the main program.

Requirements:
===========================================================
Visual Studio 2015,
C# 6 Enabled,
.NET Framework 4.6

Also: 
PM> Install-Package Ninject -Version 3.2.2