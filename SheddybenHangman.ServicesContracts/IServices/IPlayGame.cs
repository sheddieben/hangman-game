﻿using System.Collections.Generic;
using SheddyBenHangman.Core.Enums;

namespace SheddybenHangman.ServicesContracts.IServices
{
    public interface IPlayGame
    {
        void Play();
        bool AddGuessedLetters(char letter);

        bool CheckTheGuessedLetter(string letter);

        string BuildString(IEnumerable<string> inPutString, bool space);

        void DrawHangMan();
        GameStatus Result();
        void AnimateText(string word, int delay);
        void DisplayAnimatedText(string word, int delay, bool visible);
        void DisplayGameInstructions();
        bool IsLetterOnly(char letter);
    }
}
